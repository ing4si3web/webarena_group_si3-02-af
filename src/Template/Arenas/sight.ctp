<?php $this->assign('title', 'Sight');?>

<section class="col-md-12 cadreprincipal">
<section id="displayMap" class ="col-md-8">
<table id="arena">
    <?php foreach($arenaMap as $i => $line){
        echo '<tr>';
        if(isset($line[1])){
            foreach ($line as $j => $square){
                echo '<td>';
                $dist = abs($i - $sessionFighter['coordinate_x']) + abs($j - $sessionFighter['coordinate_y']);
                if($dist < 0){ $dist = 0 - $dist; }
                if($dist > $sessionFighter['skill_sight']){
                    echo $this->Html->image('fog.jpg');
                }else{
                    if(gettype($square) == 'integer'){
                    echo $this->Html->image('ground.jpg');
                    }else{
                        if(isset($square['player_id'])){
                            echo $this->Html->image('fighter' . $square['id'] . '.jpg', ['alt' => $square['name']]);
                        }else{
                            if(isset($square['bonus'])){
                                echo $this->Html->image($square['type'] . $square['bonus'] . '.jpg', ['alt' => 'weapon' . $square['bonus']]);
                            }
                        }
                    }
                }
                
                echo '</td>';
            }
        }
        echo '</tr>';
    }?>
</table>
</section>

<section id="controls" class="col-md-4">
    <div class="form-group">
        <h3>Moves : </h3>
    </div>
    <section class="col-md-12" id= "noborder"><?php   echo $this->Form->postButton('<span class="glyphicon glyphicon-arrow-up"></span>', '/arenas/sight', ['value' => 'north', 'class' => 'btn btn-primary btn-block', 'name' => 'dir']);?></section>
    <section class="col-md-6" id="noborder"><?php     echo $this->Form->postButton('<span class="glyphicon glyphicon-arrow-left"></span>', '/arenas/sight', ['value' => 'west', 'class' => 'btn btn-primary btn-block','name' => 'dir']);?></section>
    <section class="col-md-6" id="noborder"><?php     echo $this->Form->postButton('<span class="glyphicon glyphicon-arrow-right"></span>', '/arenas/sight', ['value' => 'east', 'class' => 'btn btn-primary btn-block','name' => 'dir']);?></section>
    <section class="col-md-12" id="noborder"><?php   echo $this->Form->postButton('<span class="glyphicon glyphicon-arrow-down"></span>', '/arenas/sight', ['value' => 'south', 'class' => 'btn btn-primary btn-block','name' => 'dir']);?></section>
</section>

<section id="attack" class="col-md-4">
     <div class="form-group">
        <h3>Attacks : </h3>
    </div>
    <section class="col-md-4" id= "noborder"><?php      echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 'n-w', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-4" id= "noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 'n', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-4" id= "noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 'n-e', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-6" id= "noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 'w', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-6" id= "noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 'e', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-4" id= "noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 's-w', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-4" id="noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 's', 'class' => 'btn btn-primary btn-block', 'name' => 'att']); ?> </section>
    <section class="col-md-4" id="noborder"><?php        echo $this->Form->postButton('<span class="glyphicon glyphicon-remove"></span>', '/arenas/sight', ['value' => 's-e', 'class' => 'btn btn-primary btn-block', 'name' => 'att']);?> </section>
</section>

<section id="mapState">
    <p><?php if(isset($attack)){
        echo $attack;
        if($attack === 'Attack successful! '){echo $levelUp;}
    }?></p>
    <table>
        <?php foreach($arenaMap as $i => $line){
            if(isset($line[1])){
                foreach ($line as $j => $square){
                    if(gettype($square) != 'integer'){
                        if(isset($square['player_id'])){
                            echo $this->Html->tableCells([
                            [$this->Html->image('fighter' . $square['id'] . '.jpg', ['alt' => $square['name']]), $square['name'], 'Lvl ' . $square['level'], 'Life Points : ' . $square['current_health'] . '/' . $square['skill_health']]
                        ]);
                        }
                    }
                }
            }
        }?>
    </table>
</section>



<section id="loot">
    <?php if($loot){
        echo $this->Form->postButton('Equip this:', '/arenas/sight', ['value' => 'y','name' => 'loot']);
        echo $this->Html->image($loot['type'] . $loot['bonus'] . '.jpg', ['alt' => $loot['name']]);
        echo 'Bonus: +' . $loot['bonus'] . ' ' . $loot['type'];
    }else{
        echo 'Nothing to loot here!';
    }?>
</section>
</section>