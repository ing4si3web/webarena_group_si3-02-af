<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php echo $this->Html->css('bootstrap.min.css');?>
    <?php echo $this->Html->script('jquery.js');?>
    <?php echo $this->Html->script('bootstrap.min.js');?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <li><a target="_blank" href="http://book.cakephp.org/3.0/">Documentation</a></li>
                <li><a target="_blank" href="http://api.cakephp.org/3.0/">API</a></li>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <header class="navbar navbar-inverse">
            
            <ul class="nav navbar-nav">
              <li ><?php echo $this->Html->link('Home', array('controller' => 'Arenas', 'action' => 'index')); ?></li>
              <li ><?php echo $this->Html->link('Login', array('controller' => 'Arenas', 'action' => 'login')); ?></li>
              <li ><?php echo $this->Html->link('Fighter', array('controller' => 'Arenas', 'action' => 'fighter')); ?></li>
              <li ><?php echo $this->Html->link('Diary', array('controller' => 'Arenas', 'action' => 'diary')); ?></li>
              <li ><?php echo $this->Html->link('Sight', array('controller' => 'Arenas', 'action' => 'sight')); ?></li>
            </ul>
        
    </header>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer class="navbar navbar-inverse" style="color:white;">
        <!-- <div class="row col-md-12"> -->
            <div class="col-md-6">
                <h1>Groupe SI3-02-AF</h1>
                <small>Alexandre DESCHAUME, Charles ANDRE, Valentin CLUZEAU, Gauthier LEONARD</small>
            </div>
            <div class="col-md-6 text-right">
                 <ul style="list-style-type:none; "><h3>Chosen options</h3>
                    <li>A: Fighters and tools advanced management</li>
                    <li>F: Using Bootstrap</li>
                    <li>G: Using an external connection Google/Facebook</li>
                </ul>
            </div>
        <!-- </div> -->
    </footer>
</body>
</html>
