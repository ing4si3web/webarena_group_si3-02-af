<?php
namespace App\Controller;

use App\Controller\AppController;


class WebarenaController  extends AppController
{
    public function index() {
        $this->loadModel('Fighters');
        $fighterlist = $this->Fighters->find('all');
        $this->set('fighters_content', $fighterlist->toArray());
    }
}
