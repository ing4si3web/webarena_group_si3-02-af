<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class FightersTable extends Table
{
    //returns the highest level fighter
    public function getBestFighter(){
        $temp = $this->find('all', array('order' => array('level' => 'desc')));
        return $temp->first();
    }
    
    public function getFighter($id){
        $temp = $this->find('all', array('conditions' => array('id' => $id)));
        return $temp->first();
    }
    
    //returns a table with all the information about $fighter
    public function getInfo($fighter){
        $temp = $this->find('all', array('conditions' => array('id' => $fighter)));
        return $temp->first();
    }
    
    //returns a table with all the fighters of $player
    public function getFighters($player){
        $temp = $this->find('all', array('conditions' => array('player_id' => $player)));
        return $temp;
    }
    
    //Inserts in the $map the fighters that are currently playing and returns $map
    public function displayFighters($map){
        $temp = $this->find('all');
        foreach($temp as $fighter){
            if($fighter['coordinate_x'] != -1 && $fighter['coordinate_y'] != -1){
                $map[$fighter['coordinate_x']][$fighter['coordinate_y']] = $fighter;
            }
        }
        return $map;
    }
    
    //calculating the availables skill points
    public function calcP($fighter, $tools){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $lvl = $f['level'];
            if(isset($tools['ring']['bonus'])){
                $sightP = $f['skill_sight'] - $tools['ring']['bonus'];
            }else{ $sightP = $f['skill_sight']; }
            if(isset($tools['weapon']['bonus'])){
                $strenghtP = $f['skill_strength'] - $tools['weapon']['bonus'];
            }else{ $strenghtP = $f['skill_strength']; }
            if(isset($tools['armor']['bonus'])){
                $healthP = $f['skill_health'] - $tools['armor']['bonus'];
            }else{ $healthP = $f['skill_health']; }
            $skillP = $lvl - 1 - $sightP + 1 - $strenghtP + 1 - ($healthP - 3)/3;
            return $skillP;
        }
        else{
            return false;
        }
    }
    
    //calling the right moving function
    public function move($dir, $fighter){
        switch($dir){
            case "west": $this->moveW($fighter);
                break;
            case "north": $this->moveN($fighter);
                break;
            case "south": $this->moveS($fighter);
                break;
            case "east": $this->moveE($fighter);
                break;
            default: break;
        }
    }
    
    //moving west
    public function moveW($fighter){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $y = $f['coordinate_y'] - 1;
            $x = $f['coordinate_x'];
            $cond = array('coordinate_x' => $x, 'coordinate_y' => $y);
            if($this->exists($cond)){
                return;
            }
            if($f['coordinate_y'] != 0){
                $f['coordinate_y'] = $f['coordinate_y'] - 1;
                $this->save($f);
            }
        }
    }
    
    //moving east
    public function moveE($fighter){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $y = $f['coordinate_y'] + 1;
            $x = $f['coordinate_x'];
            $cond = array('coordinate_x' => $x, 'coordinate_y' => $y);
            if($this->exists($cond)){
                return;
            }
            if($f['coordinate_y'] != 14){
                $f['coordinate_y'] = $f['coordinate_y'] + 1;
                $this->save($f);
            }
        }
    }
    
    //moving north
    public function moveN($fighter){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $y = $f['coordinate_y'];
            $x = $f['coordinate_x'] - 1;
            $cond = array('coordinate_x' => $x, 'coordinate_y' => $y);
            if($this->exists($cond)){
                return;
            }
            if($f['coordinate_y'] != 0){
                $f['coordinate_x'] = $f['coordinate_x'] - 1;
                $this->save($f);
            }
        }
    }
    
    //moving south
    public function moveS($fighter){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $y = $f['coordinate_y'];
            $x = $f['coordinate_x'] + 1;
            $cond = array('coordinate_x' => $x, 'coordinate_y' => $y);
            if($this->exists($cond)){
                return;
            }
            if($f['coordinate_y'] != 0){
                $f['coordinate_x'] = $f['coordinate_x'] + 1;
                $this->save($f);
            }
        }
    }
    
    //calling the attack function depending on the direction
    public function att($dir, $fighter){
        switch($dir){
            case 'n-w': return $this->attack($fighter, -1, -1);
            case 'n': return $this->attack($fighter, -1, 0);
            case 'n-e': return $this->attack($fighter, -1, 1);
            case 'w': return $this->attack($fighter, 0, -1);
            case 'e': return $this->attack($fighter, 0, 1);
            case 's-w': return $this->attack($fighter, 1, -1);
            case 's': return $this->attack($fighter, 1, 0);
            case 's-e': return $this->attack($fighter, 1, 1);
        }
    }
    
    //returning the fighter to attack if it exists
    public function getDirFighter($x, $y, $dir){
        switch($dir){
            case 'n-w': if($this->exists(array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y - 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y - 1))));
                    return $temp->first();
                }else{ return false; }
            case 'n': if($this->exists(array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y))));
                    return $temp->first();
                }else{ return false; }
            case 'n-e': if($this->exists(array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y + 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x - 1), 'coordinate_y' => ($y + 1))));
                    return $temp->first();
                }else{ return false; }
            case 'w': if($this->exists(array('coordinate_x' => ($x), 'coordinate_y' => ($y - 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x), 'coordinate_y' => ($y - 1))));
                    return $temp->first();
                }else{ return false; }
            case 'e': if($this->exists(array('coordinate_x' => ($x), 'coordinate_y' => ($y + 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x), 'coordinate_y' => ($y + 1))));
                    return $temp->first();
                }else{ return false; }
            case 's-w': if($this->exists(array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y - 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y - 1))));
                    return $temp->first();
                }else{ return false; }
            case 's': if($this->exists(array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y))));
                    return $temp->first();
                }else{ return false; }
            case 's-e': if($this->exists(array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y + 1)))){
                    $temp = $this->find('all', array('conditions' => array('coordinate_x' => ($x + 1), 'coordinate_y' => ($y + 1))));
                    return $temp->first();
                }else{ return false; }
        }
    }
    
    //testing if someone to attack and doing the random success algorithm
    public function success($fighter, $dir){
        $temp = $this->find('all', array('conditions' => array('id' => $fighter)));
        $attacking = $temp->first();
        $attacked = $this->getDirFighter($attacking['coordinate_x'], $attacking['coordinate_y'], $dir);
        if($attacked){
            if(rand(1, 20) > (10 + $attacked['level'] - $attacking['level'])){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    //level up fighter if needed
    public function lvlUp($f){
        if(($f['xp'] + 4 - $f['level'] * 4) == 4){
            $f['level'] = $f['level'] + 1;
            $this->save($f);
            return 'Your fighter leveled up!';
        }else{
            return '';
        }
    }
    
    //processing the attack and death if health <= 0
    public function attack($fighter, $x, $y){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            $y = $f['coordinate_y'] + $y;
            $x = $f['coordinate_x'] + $x;
            $cond = array('coordinate_x' => $x, 'coordinate_y' => $y);
            if($this->exists($cond)){
                $temp = $this->find('all', array('conditions' => $cond));
                $f2 = $temp ->first();
                $f2['current_health'] = $f2['current_health'] - $f['skill_strength'];
                $f['xp'] = $f['xp'] + 1;
                if($f2['current_health'] > 0){
                    $this->save($f2);
                }else{
                    $this->deleteAll($cond, false);
                }
                $this->save($f);
                return $this->lvlUp($f);
            }
        }
    }
    
    //processing the changes in skills amount depending on the item equiped
    public function updateBonus($fighter, $bonusDif){
        $conditions = array('id' => $fighter);
        if($this->exists($conditions)){
            $temp = $this->find('all', array('conditions' => $conditions));
            $f = $temp->first();
            switch($bonusDif['type']){
                case 'weapon': $f['skill_strength'] = $f['skill_strength'] + $bonusDif['dif'];
                    break;
                case 'armor': $f['skill_health'] = $f['skill_health'] + $bonusDif['dif'];
                    $f['current_health'] = $f['current_health'] + $bonusDif['dif'];
                    break;
                case 'ring': $f['skill_sight'] = $f['skill_sight'] + $bonusDif['dif'];
                    break;
                default : break;
            }
            $this->save($f);
        }
    }
}