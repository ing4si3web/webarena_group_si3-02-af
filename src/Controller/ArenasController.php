<?php

namespace App\Controller;

use App\Controller\AppController;

class ArenasController extends AppController {

    public function index() {
        $this->loadModel('Fighters');
        $this->set('myname', "Julien Falconnet");
        $this->set('bestFighter', $this->Fighters->getBestFighter());
    }

    public function login() {
        $this->loadModel('Players');
        if ($this->request->is('post') && $this->request->data['email']!=NULL && $this->request->data['password']!=NULL) {
            if($playerId = $this->Players->checkInfos($this->request->data['email'], $this->request->data['password'])) {
                $this->request->session()->write('playerId', $playerId);
                $this->redirect(['action' => 'fighter']);
            }
        else {$this->Flash->error('Wrong login, please try again !');}
        }
    }

    public function logout() {
        $this->request->session()->delete('playerId');
        $this->redirect(['action' => '/']);
    }

    public function fighter() {
        // Check if the user is logged
        if ($this->request->session()->check('playerId')==NULL) {$this->redirect(['action' => 'login']);}

        //loading table models
        $this->loadModel('Fighters');
        $this->loadModel('Tools');
        
        //setting players fighterList and current selectedFighter from database
        $this->set('fightersList', $this->Fighters->getFighters('545f827c-576c-4dc5-ab6d-27c33186dc3e'));
        //TODO: when login, ask for and store selectedFighter in session variable
        $this->set('selectedFighter', $this->Fighters->getFighters('545f827c-576c-4dc5-ab6d-27c33186dc3e')->first());
        
        //calculating the number of skill points availables and setting $skillP
        $tools = $this->Tools->getFighterTools(1);
        $this->set('skillP', $this->Fighters->calcP(1, $tools));
        
        //setting fighter's equipement from database
        $this->set('weapon', $tools['weapon']);
        $this->set('armor', $tools['armor']);
        $this->set('ring', $tools['ring']);
    }

    public function sight() {
        // Check if the user is logged
        if ($this->request->session()->check('playerId')==NULL) {$this->redirect(['action' => 'login']);}

        //loading table models
        $this->loadModel('Fighters');
        $this->loadModel('Tools');
        $this->loadModel('Events');
        
        //creating the game map
        $map = array();
        for ($i=0; $i<10; $i++){
            $map[$i] = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        }
        
        //moving the session fighter if needed
        $dir = $this->request->data('dir');
        if($dir){
            $this->Fighters->move($dir, 1);
            $this->Events->move($dir, $this->Fighters->getFighter(1));
            //$this->request->session()->read('fighterId')
        }
        
        //attacking if needed
        $att = $this->request->data('att');
        if($att){
            if($this->Fighters->success(1, $att)){
                $lvlUp = $this->Fighters->att($att, 1);
                $this->set('levelUp', $lvlUp);
                $this->Events->attack(true, $this->Fighters->getFighter(1), $lvlUp, $att);
                $this->set('attack', 'Attack successful! ');
            }else{
                $this->set('attack', 'Attack failed!');
                $this->Events->attack(false, $this->Fighters->getFighter(1), false, $att);
            }
        }
        
        //equiping the new item, dropping the previous one and updating fighter skills
        $equip = $this->request->data('loot');
        if($equip){
            $bonusDif = $this->Tools->equip($this->Fighters->getFighters("545f827c-576c-4dc5-ab6d-27c33186dc3e")->first());
            $this->Fighters->updateBonus(1, $bonusDif);
            $this->Events->loot($this->Fighters->getFighter(1), $bonusDif);
        }
        
        //setting sessionFighter with the player's fighter
        $this->set('sessionFighter', $this->Fighters->getFighters("545f827c-576c-4dc5-ab6d-27c33186dc3e")->first());
        
        //adding the playing fighters to the map
        $mapWithTools = $this->Tools->displayTools($map);
        //adding the unequiped tools to the map and setting the game map
        $this->set('arenaMap', $this->Fighters->displayFighters($mapWithTools));
        
        //setting loot with the equipment the session fighter can loot
        $this->set('loot', $this->Tools->checkLoot($this->Fighters->getFighters("545f827c-576c-4dc5-ab6d-27c33186dc3e")->first()));
    }

    public function diary() {
        // Check if the user is logged
        if ($this->request->session()->check('playerId')==NULL) {$this->redirect(['action' => 'login']);}
        
        $this->loadModel('Events');
        
        $this->set('events', $this->Events->getEvents());
    }

}
