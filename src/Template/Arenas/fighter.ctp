<?php $this->assign('title', 'Fighter');
    $xp = $selectedFighter['xp'] - ($selectedFighter['level']-1)*4;
?>
<head>
    <?php
        echo $this->Html->css("bootstrap.min");
        echo $this->Html->css("fighter");
    ?>
</head>
<section class="col-md-12 cadreprincipal">
<section class="col-md-4">
<section class=" col-md-12 cadresecondaire">
    <table id="fighters" class="table table-striped">
        <thead><tr><th>Your Fighters</th></tr></thead>
        <tbody><?php
            foreach ($fightersList as $f){
                echo $this->Html->tableCells([
                    [$this->Html->image('fighter' . $f['id'] . '.jpg', ["class" => "img-circle",'alt' => $f['name']]), $f['name'], 'Lvl ' . $f['level']]
                ]);
            }?>
            
        </tbody>
    </table>   
</section>
</section>

<section class="col-md-8">
<section class=" col-md-12 cadresecondaire">
    <table id="fighterDetails" class="table table-striped">
        <thead>
     
        <div class ='col-md-6' id="image"> <?php echo $this->Html->image('fighter' . $selectedFighter['id'] . '.jpg', array("class" => "img-circle", "alt" => $selectedFighter['name'])) ?></div>
        <h1 class ='col-md-6'> <?php echo $selectedFighter['name']?> </h1>
        <h2><?php echo 'Skill points: ' . $skillP?></h2>
           
        </thead>
        <tbody>
            <tr>
                <td>Level</td>
                <td><?php echo $selectedFighter['level']?></td>
                <td><?php echo $selectedFighter['level']?> <progress class="progress progress-info" value="<?php echo $xp?>" max="4"></progress> <?php echo $selectedFighter['level']+1?></td>
            </tr>
            <tr>
                <td>View</td>
                <td><?php echo $selectedFighter['skill_sight']?></td>
                <td><button class="btn btn-default">+1</button></td>
            </tr>
            <tr>
                <td>Strength</td>
                <td><?php echo $selectedFighter['skill_strength']?></td>
                <td><button class="btn btn-default">+1</button></td>
            </tr>
            <tr>
                <td>Health Points</td>
                <td><?php echo $selectedFighter['current_health']?>/<?php echo $selectedFighter['skill_health']?></td>
                <td><button class="btn btn-default">+3</button></td>
            </tr>
        </tbody>
    </table>
    </section>
    <section class=" col-md-12 cadresecondaire">
    <table id="fighterTools" class="table table-striped">
        <thead>
            <tr>
                <th><?php echo $selectedFighter['name'];?>'s Tools</th>
            </tr>
        </thead>
        <tbody>
            <?php echo $this->Html->tableCells([
            ['Weapon', $this->Html->image('weapon' . $weapon['bonus'] . '.jpg', ['alt' => 'Weapon lvl ' . $weapon['bonus']]), 'Strenght +' . $weapon['bonus']],
            ['Armor', $this->Html->image('armor' . $armor['bonus'] . '.jpg', ['alt' => 'Armor lvl ' . $armor['bonus']]), 'H.P. +' . $armor['bonus']],
            ['Ring', $this->Html->image('ring' . $ring['bonus'] . '.jpg', ['alt' => 'Ring lvl ' . $ring['bonus']]), 'View +' . $ring['bonus']]
            ]);?>
        </tbody>
    </table>
    
</section>
</section>
</section>
